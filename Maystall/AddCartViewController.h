//
//  AddCartViewController.h
//  Maystall
//
//  Created by DAWEI FAN on 10/03/2014.
//  Copyright (c) 2014 huiztech. All rights reserved.
//

#import "BaseViewController.h"

@interface AddCartViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSString *totalPrice_String;
    UILabel *label_totalPrice;
    UILabel *label_Price;
    
    UITableView *mainTableView;
    
    
}

@end
